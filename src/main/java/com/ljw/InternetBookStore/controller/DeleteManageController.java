package com.ljw.InternetBookStore.controller;

import com.ljw.InternetBookStore.model.CommonResult;
import com.ljw.InternetBookStore.service.DeleteManageService;
import com.ljw.InternetBookStore.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/delete")
public class DeleteManageController {
    private final DeleteManageService deleteManageService;

    @DeleteMapping("/{memberId}")
    public CommonResult delMember(long memberId) {
        deleteManageService.delMember(memberId);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{bookId}")
    public CommonResult delBook(long bookId) {
        deleteManageService.delBook(bookId);

        return ResponseService.getSuccessResult();
    }
}
