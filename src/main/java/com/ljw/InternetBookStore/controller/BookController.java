package com.ljw.InternetBookStore.controller;

import com.ljw.InternetBookStore.model.BookRequest;
import com.ljw.InternetBookStore.model.CommonResult;
import com.ljw.InternetBookStore.service.BookService;
import com.ljw.InternetBookStore.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/book")
public class BookController {
    private final BookService bookService;

    @PostMapping("/new")
    public CommonResult setBook(@RequestBody @Valid BookRequest request) {
        bookService.setBook(request);

        return ResponseService.getSuccessResult();
    }
}
