package com.ljw.InternetBookStore.controller;

import com.ljw.InternetBookStore.entity.Member;
import com.ljw.InternetBookStore.model.CommonResult;
import com.ljw.InternetBookStore.model.MemberRequest;
import com.ljw.InternetBookStore.model.SingleResult;
import com.ljw.InternetBookStore.service.MemberService;
import com.ljw.InternetBookStore.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/{id}")
    public SingleResult<Member> getMember(@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMember(id));
    }
}
