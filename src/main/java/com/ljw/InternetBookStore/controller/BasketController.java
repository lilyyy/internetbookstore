package com.ljw.InternetBookStore.controller;

import com.ljw.InternetBookStore.entity.Book;
import com.ljw.InternetBookStore.entity.Member;
import com.ljw.InternetBookStore.model.BasketItem;
import com.ljw.InternetBookStore.model.BasketRequest;
import com.ljw.InternetBookStore.model.CommonResult;
import com.ljw.InternetBookStore.model.ListResult;
import com.ljw.InternetBookStore.service.BasketService;
import com.ljw.InternetBookStore.service.BookService;
import com.ljw.InternetBookStore.service.MemberService;
import com.ljw.InternetBookStore.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/basket")
public class BasketController {
    private final BasketService basketService;
    private final MemberService memberService;
    private final BookService bookService;

    @PostMapping("/member-id/{memberId}/book-id/{bookId}")
    public CommonResult setBasket(@PathVariable long memberId, @PathVariable long bookId, @RequestBody @Valid BasketRequest request) {
        Member member = memberService.getMember(memberId);
        Book book = bookService.getBook(bookId);
        basketService.setBasket(member, book, request.getBookCount());

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    public ListResult<BasketItem> getBaskets() {
        return ResponseService.getListResult(basketService.getBaskets(), true);
    }

    @PutMapping("/info/{basketId}")
    public CommonResult putBasketInfo(@PathVariable long basketId, @RequestBody @Valid BasketRequest request) {
        basketService.putBasketInfo(basketId, request.getBookCount());

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/basket-id/{basketId}/member-id/{memberId}")
    public CommonResult putBasketMember(long basketId, long memberId) {
        Member member = memberService.getMember(memberId);
        basketService.putBasketMember(basketId, member);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/basket-id/{basketId}/book-id/{bookId}")
    public CommonResult putBasketBook(long basketId, long bookId) {
        Book book = bookService.getBook(bookId);
        basketService.putBasketBook(basketId, book);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{basketId}")
    public CommonResult delBasket(long basketId) {
        basketService.delBasket(basketId);

        return ResponseService.getSuccessResult();
    }
}
