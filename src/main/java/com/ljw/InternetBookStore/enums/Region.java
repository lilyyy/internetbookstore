package com.ljw.InternetBookStore.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Region {
    KOREA("국내")
    , OVERSEAS("해외")
    ;

    private final String name;
}
