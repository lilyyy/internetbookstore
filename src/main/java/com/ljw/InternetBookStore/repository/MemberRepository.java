package com.ljw.InternetBookStore.repository;

import com.ljw.InternetBookStore.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
