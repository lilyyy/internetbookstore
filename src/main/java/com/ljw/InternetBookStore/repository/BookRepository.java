package com.ljw.InternetBookStore.repository;

import com.ljw.InternetBookStore.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
