package com.ljw.InternetBookStore.repository;

import com.ljw.InternetBookStore.entity.Basket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BasketRepository extends JpaRepository<Basket, Long> {
    List<Basket> findAllByMember_Id(long memberId);
    List<Basket> findAllByBook_Id(long bookId);
}
