package com.ljw.InternetBookStore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InternetBookStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(InternetBookStoreApplication.class, args);
	}

}
