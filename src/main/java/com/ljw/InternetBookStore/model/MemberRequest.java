package com.ljw.InternetBookStore.model;

import com.ljw.InternetBookStore.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberRequest {
    @NotNull
    @Length(max = 40)
    private String email;

    @NotNull
    @Length(min = 8, max = 20)
    private String password;

    @NotNull
    @Length(min = 12, max = 30)
    private String phoneNum;

    @NotNull
    @Length(min = 2,max = 20)
    private String memberName;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @NotNull
    @Length(max = 50)
    private String address;

    @NotNull
    @Length(max = 30)
    private String refundAccount;

    @NotNull
    @Length(max = 10)
    private String bank;
}
