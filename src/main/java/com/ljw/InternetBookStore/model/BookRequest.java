package com.ljw.InternetBookStore.model;

import com.ljw.InternetBookStore.enums.Region;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class BookRequest {
    @NotNull
    @Length(max = 100)
    private String bookName;

    @NotNull
    @Length(min = 2, max = 20)
    private String author;

    @NotNull
    @Length(min = 2, max = 20)
    private String translator;

    @NotNull
    @Length(max = 20)
    private String genre;

    @NotNull
    @Length(max = 30)
    private String publisher;

    @NotNull
    private LocalDate datePublish;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Region region;

    @NotNull
    private Double price;
}
