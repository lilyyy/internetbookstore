package com.ljw.InternetBookStore.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasketItem {
    private Long id;

    private Long memberId; // Member

    private Long bookId; // Book

    private String memberName; // Member

    private String bookName; // Book

    private String bookAuthor; // Book

    private Integer bookCount;
}
