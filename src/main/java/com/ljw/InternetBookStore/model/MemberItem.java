package com.ljw.InternetBookStore.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private Long id;

    private String email;

    private String memberName;
}
