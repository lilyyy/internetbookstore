package com.ljw.InternetBookStore.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BasketRequest {
    @NotNull
    private Integer bookCount;
}
