package com.ljw.InternetBookStore.entity;

import com.ljw.InternetBookStore.enums.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 40)
    private String email;

    @Column(nullable = false, length = 20)
    private String password;

    @Column(nullable = false, length = 30)
    private String phoneNum;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.ORDINAL)
    private Gender gender;

    @Column(nullable = false, length = 50)
    private String address;

    @Column(nullable = false)
    private Double mileage;

    @Column(nullable = false, length = 30)
    private String refundAccount;

    @Column(nullable = false, length = 20)
    private String bank;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;
}
