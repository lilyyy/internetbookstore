package com.ljw.InternetBookStore.entity;

import com.ljw.InternetBookStore.enums.Region;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 100)
    private String bookName;

    @Column(nullable = false, length = 20)
    private String author;

    @Column(nullable = false, length = 20)
    private String translator;

    @Column(nullable = false, length = 20)
    private String genre;

    @Column(nullable = false, length = 30)
    private String publisher;

    @Column(nullable = false)
    private LocalDate datePublish;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private Region region;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;
}
