package com.ljw.InternetBookStore.service;

import com.ljw.InternetBookStore.entity.Book;
import com.ljw.InternetBookStore.exception.CMissingDataException;
import com.ljw.InternetBookStore.model.BookRequest;
import com.ljw.InternetBookStore.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    public void setBook(BookRequest request) {
        Book book = new Book();
        book.setBookName(request.getBookName());
        book.setAuthor(request.getAuthor());
        book.setTranslator(request.getTranslator());
        book.setGenre(request.getGenre());
        book.setPublisher(request.getPublisher());
        book.setDatePublish(request.getDatePublish());
        book.setRegion(request.getRegion());
        book.setPrice(request.getPrice());
        book.setDateCreate(LocalDateTime.now());
        book.setDateUpdate(LocalDateTime.now());

        bookRepository.save(book);
    }

    public Book getBook(long id) {
        return bookRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}
