package com.ljw.InternetBookStore.service;

import com.ljw.InternetBookStore.entity.Member;
import com.ljw.InternetBookStore.exception.CMissingDataException;
import com.ljw.InternetBookStore.model.MemberItem;
import com.ljw.InternetBookStore.model.MemberRequest;
import com.ljw.InternetBookStore.model.SingleResult;
import com.ljw.InternetBookStore.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public void setMember(MemberRequest request) {
        Member member = new Member();
        member.setEmail(request.getEmail());
        member.setPassword(request.getPassword());
        member.setGender(request.getGender());
        member.setRefundAccount(request.getRefundAccount());
        member.setBank(request.getBank());
        member.setMileage(0.0);
        member.setDateCreate(LocalDateTime.now());
        member.setDateUpdate(LocalDateTime.now());

        memberRepository.save(member);
    }

    public Member getMember(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public List<MemberItem> getMembers() {
        List<MemberItem> result = new LinkedList<>();

        List<Member> members = memberRepository.findAll();

        for (Member member : members) {
            MemberItem newItem = new MemberItem();
            newItem.setId(member.getId());
            newItem.setMemberName(member.getMemberName());
            newItem.setEmail(member.getEmail());

            result.add(newItem);
        }

        return result;
    }
}
