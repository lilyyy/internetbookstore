package com.ljw.InternetBookStore.service;

import com.ljw.InternetBookStore.entity.Basket;
import com.ljw.InternetBookStore.entity.Book;
import com.ljw.InternetBookStore.entity.Member;
import com.ljw.InternetBookStore.exception.CMissingDataException;
import com.ljw.InternetBookStore.model.BasketItem;
import com.ljw.InternetBookStore.model.BasketRequest;
import com.ljw.InternetBookStore.model.ListResult;
import com.ljw.InternetBookStore.repository.BasketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BasketService {
    private final BasketRepository basketRepository;

    public void setBasket(Member member, Book book, int bookCount) {
        Basket basket = new Basket();
        basket.setMember(member);
        basket.setBook(book);
        basket.setBookCount(bookCount);
        basket.setDateCreate(LocalDateTime.now());
        basket.setDateUpdate(LocalDateTime.now());

        basketRepository.save(basket);
    }

    public ListResult<BasketItem> getBaskets() {
        List<BasketItem> result = new LinkedList<>();

        List<Basket> baskets = basketRepository.findAll();

        for (Basket basket : baskets) {
            BasketItem addItem = new BasketItem();
            addItem.setId(basket.getId());
            addItem.setMemberId(basket.getMember().getId());
            addItem.setBookId(basket.getBook().getId());
            addItem.setMemberName(basket.getMember().getMemberName());
            addItem.setBookName(basket.getBook().getBookName());
            addItem.setBookAuthor(basket.getBook().getAuthor());
            addItem.setBookCount(basket.getBookCount());

            result.add(addItem);
        }
        return ListConvertService.settingResult(result);
    }

    public void putBasketInfo(long basketId, int bookCount) {
        Basket basket = basketRepository.findById(basketId).orElseThrow(CMissingDataException::new);
        basket.setBookCount(bookCount);

        basketRepository.save(basket);
    }

    public void putBasketMember(long basketId, Member member) {
        Basket basket = basketRepository.findById(basketId).orElseThrow(CMissingDataException::new);
        basket.setMember(member);

        basketRepository.save(basket);
    }

    public void putBasketBook(long basketId, Book book) {
        Basket basket = basketRepository.findById(basketId).orElseThrow(CMissingDataException::new);
        basket.setBook(book);

        basketRepository.save(basket);
    }

    public void delBasket(long basketId) {
        basketRepository.deleteById(basketId);
    }
}
