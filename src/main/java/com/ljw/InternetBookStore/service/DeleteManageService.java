package com.ljw.InternetBookStore.service;


import com.ljw.InternetBookStore.entity.Basket;
import com.ljw.InternetBookStore.entity.Book;
import com.ljw.InternetBookStore.repository.BasketRepository;
import com.ljw.InternetBookStore.repository.BookRepository;
import com.ljw.InternetBookStore.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteManageService {
    private final BasketRepository basketRepository;
    private final MemberRepository memberRepository;
    private final BookRepository bookRepository;

    public void delMember(long memberId) {
        List<Basket> baskets = basketRepository.findAllByMember_Id(memberId);
        for (Basket basket : baskets) {
            basketRepository.deleteById(basket.getId());
        }

        memberRepository.deleteById(memberId);
    }

    public void delBook(long bookId) {
        List<Basket> baskets = basketRepository.findAllByBook_Id(bookId);
        for (Basket basket : baskets) {
            basketRepository.deleteById(basket.getId());
        }

        bookRepository.deleteById(bookId);
    }
}
